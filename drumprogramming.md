# two different drum SW categories

## drum VSTs

- mododrum
- spitfire
- manda audio
- steven slate
- addictive drums
- ...

## drum sampler (MPC style)

- speedrum lite
- speedrum
- bitwig drum machine
- NI battery
- sitala
- ...

# definitions

## L/C/S 

- "Loop / Chop / Singles"
- Loop (classically from vinyl) --> Chop --> Single (original) samples

##  L/C/L 

- "Loop / Chop / Loopsnippets"
- Loop (from own workflow or vinyl) --> Chop --> snippets of the Loop (w/ a length of 8th or 16th each)

# workflow

Two flows: one is from a loop (eg from vinyl), the other is from a drum VST (w/ sampled real drums or from drum-samplers, classic or modern).

Both flows end up in L/C/L, which is consolidated, rendered and simplified for further use in resampling phase.

    midi --\
    L/C/S ---> drum sampler --> audio --> DAW processing ---\
                                                             \
    midi --> drum VST --> audio --> DAW processing  -----------> L/C/L ---> drum sampler --> audio
                                                                        ^
                                                                 midi --|
                                                             |
              LOOP CREATION PHASE                            |          RESAMPLING PHASE

notes:

- drum sampler and VSTs may already output processed audio
  - i.e. amp ADSR, filter ADSR, comp, transient shaping, distortion - all on a per cell base
- drum sampler and VSTs may output via multiple audio channels (e.g. 16)
  - here, processing w/ DAW (native and VSTs) can take place

## layering

- within flow 1: have multiple samples (eg snares) in the drum sampler, trigger them simultaneously via midi
- flow 1 and flow 2 together: use the same midi for both flows (i.e. for drum sampler and drum VST) and record them simultaneously in the Loop Creation Phase

## conclusion

For one song, have 2 projects: one for loop creation and use another project for the resampling phase. That one would be for the big musical idea - the song structure - and it contains the melodic (or non-melodic) elements.

## future prospects

There's a cut between both phases which demands manual effort.
Loop creation and resampling phase should be one integrated, fully automated workflow.
I.e. if I change one snare-sound in the Loop Creation Phase, I want to be able to hear the difference in the final mixdown without doing anything else than just replacing that sample at the beginning of the workflow.


# bitwig specific stuff

## grouping together audio of drum machine cells

For example, all kicks should be in one group, all snares in another etc., i.e. a dedicated bus for grouped drum sounds.

3 alternatives:

- one instrument track w/ one drum machine per group
- create a send/fx track for each group, go to mix-view, send the cells 100% to the appropriate send/fx track, mute the outputs of the drum machine
- create a track with Audio Receivers and send audio there at the end of a cell's effect chain

## clip launcher usage patterns

### scene as section (of a tune)

Possibility to render to arranger, modify/tweak it there, then consolidate (possibly multiple clips) in one, put that one back into the clip launcher.
Results in ever growing larger parts.

### scene as variant (of another scene)

Possibility to slightly change a copy of a clip.
Automation is clip local, so a variant can also be altered via different automation.
Results in ever growing number of variants.
